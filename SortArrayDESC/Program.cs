﻿namespace SortArrayDESC;

public class Program
{
    static void Main(string[] args)
    {
        var sorted = SortArrayDesc(new int[] { 1 });
        Console.WriteLine(sorted);
    }

    public static int[] SortArrayDesc(int[] x)
    {
        // sort the array in descending order
        Array.Sort(x);
        Array.Reverse(x);
        return x;
    }

}