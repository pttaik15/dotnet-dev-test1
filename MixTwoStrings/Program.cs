﻿var result = PracticeStrings.MixTwoStrings("", "");
Console.WriteLine(result);
Console.WriteLine("hello world");

public class PracticeStrings
{

    public static string MixTwoStrings(string value1, string value2)
    {
        string result = "";
        int length = value1.Length > value2.Length ? value1.Length : value2.Length;
        for (int i = 0; i < length; i++)
        {
            if (i < value1.Length)
            {
                result += value1[i];
            }
            if (i < value2.Length)
            {
                result += value2[i];
            }
        }
        return result;
    }
}

