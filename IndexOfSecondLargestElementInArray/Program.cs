﻿namespace IndexOfSecondLargestElementInArray;

public class Program
{
    static void Main(string[] args)
    {
        var myArr = new[] { 1, 2, 3 };
        var result = IndexOfSecondLargestElementInArray(myArr);
        Console.WriteLine(result);
    }

    public static int IndexOfSecondLargestElementInArray(int[] x)
    {
        // find the index of the second largest element in the array
        // if the array has less than 2 elements, return -1
        if (x.Length < 2)
        {
            return -1;
        }
        // if the array has 2 elements, return the index of the smaller element
        if (x.Length == 2)
        {
            return x[0] < x[1] ? 0 : 1;
        }
        // if there are have more than 2 elements duplicate, return the index of the first duplicate
        var max = x[0];
        var secondMax = x[1];
        var maxIndex = 0;
        var secondMaxIndex = 1;
        for (int i = 2; i < x.Length; i++)
        {
            if (x[i] > max)
            {
                secondMax = max;
                secondMaxIndex = maxIndex;
                max = x[i];
                maxIndex = i;
            }
            else if (x[i] > secondMax)
            {
                secondMax = x[i];
                secondMaxIndex = i;
            }
        }
        return secondMaxIndex;
    }
}